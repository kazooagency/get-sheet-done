import React, { useState, useEffect } from "react";

function UserListComponent() {
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  async function fetchData() {
    console.log("fetching data");
    const res = await fetch(
      "https://spreadsheets.google.com/feeds/cells/1ctOJ8UeX9bFpppN8rChJtxQIeUwuVfTQnVwxwQPe-HQ/1/public/full?alt=json"
    );
    res
      .json()
      .then(res => {
        setIsLoading(false);
        setList(jsonifyArray(res.feed.entry));
      })
      .catch(err => console.log(err));
  }

  useEffect(() => {
    if (isLoading) {
      fetchData();
    }
  });

  if (list && list[0] && list[0].name) {
    return list.map(el => (
      <div>
        <a href={"/profile/" + el.sheetId}>{el.name}</a>
        <br></br>
      </div>
    ));
  } else {
    return <p>Loading</p>;
  }
}

function jsonifyArray(array) {
  var jsonArray = [];

  for (var i = 2; i < array.length; i += 2) {
    jsonArray.push({
      name: array[i].content.$t,
      sheetId: array[i + 1].content.$t
    });
  }
  return jsonArray;
}

export default UserListComponent;
