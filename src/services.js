function jsonifyArray(array) {
  var jsonArray = [];

  for (var i = 0; i < array.length; i += 2) {
    jsonArray.push({
      title: array[i].content.$t,
      data: array[i + 1].content.$t
    });
  }
  // console.log(jsonArray);
  return jsonArray;
}

export async function fetchData(id) {
  let sheetArray = [];
  let tab = 1;
  let isok = true;
  do {
    const res = await fetch(
      "https://spreadsheets.google.com/feeds/cells/" +
        id +
        "/" +
        tab +
        "/public/full?alt=json"
    );
    if (res.status === 200) {
      const json = await res
        .json()
        .then(res => {
          // console.log("tab " + tab);
          // console.log("res");
          // console.log(res);
          tab++;
          return jsonifyArray(res.feed.entry);
        })
        .catch(err => {
          // console.log(err);
          isok = false;
        });
      sheetArray.push(json);
    } else {
      // console.log("Not 200");
      isok = false;
    }
  } while (isok);

  // console.log(sheetArray);

  return sheetArray;
}
