import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { fetchData } from "./services.js";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";

function ProfileComponent() {
  const [list, setList] = useState([]);
  const [tab, setTab] = useState(0);
  let { id } = useParams();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    (async () => {
      if (isLoading) {
        setIsLoading(false);
        setList(await fetchData(id));
      }
    })();
  }, []);

  if (list && list[tab] && list[tab][0].title) {
    return (
      <div>
        <Button onClick={() => setTab(0)}>Switch Tab 1</Button>
        <Button onClick={() => setTab(1)}>Switch Tab 2</Button>
        <Button onClick={() => setTab(2)}>Switch Tab 3</Button>
        <Table striped bordered hover>
          <tbody>
            {list[tab].map((el, index) => (
              <tr>
                <td key={index}>{el.title}</td>
                <td>{el.data}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  } else {
    return <p>Loading</p>;
  }
}

export default ProfileComponent;
