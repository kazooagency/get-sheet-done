import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import "./App.css";
import UserListComponent from "./UserListComponent";
import ProfileComponent from "./ProfileComponent";

function App() {
  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
        </ul>

        <hr />
        <Switch>
          <Route exact path="/">
            <UserListComponent></UserListComponent>
          </Route>
          <Route path="/profile/:id">
            <ProfileComponent></ProfileComponent>
          </Route>
        </Switch>
      </div>
    </Router>
  );
  // return <UserListComponent el="test"></UserListComponent>;
}

export default App;
